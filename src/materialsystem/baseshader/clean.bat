@echo off
setlocal

if exist fxctmp9 rd /s /q fxctmp9
if exist vshtmp9 rd /s /q vshtmp9
if exist pshtmp9 rd /s /q pshtmp9
if exist fxctmp9_tmp rd /s /q fxctmp9_tmp
if exist vshtmp9_tmp rd /s /q vshtmp9_tmp
if exist pshtmp9_tmp rd /s /q pshtmp9_tmp
if exist shaders rd /s /q shaders
if exist Debug_dx9 rd /s /q Debug_dx9
if exist Release_dx9 rd /s /q Release_dx9
if exist filestocopy.txt rm /s filestocopy.txt
if exist inclist.txt rm /s inclist.txt
if exist uniquefilestocopy.txt rm /s uniquefilestocopy.txt
if exist vcslist.txt rm /s vcslist.txt
if exist makefile.shaders_dx9_20b rm /s makefile.shaders_dx9_20b
if exist makefile.shaders_dx9_30 rm /s makefile.shaders_dx9_30

if /i "%1" == "-game" goto CleanGameDir

:CleanGameDir
set __GameDir=%~2
if not exist "%__GameDir%\gameinfo.txt" goto MissingGameInfo
rem if exist "%__GameDir%\shaders" rd /s /q "%2\shaders"
goto end

:MissingGameInfo
echo Invalid -game parameter specified (no "%__GameDir%\gameinfo.txt" exists).
goto end

:end