WeaponData
{
	"printname"		"#SMOD_DEAGLE"
	"viewmodel"		"models/weapons/v_deagle.mdl"
	"playermodel"		"models/weapons/w_deagle.mdl"
	"anim_prefix"		"pistol"
	"bucket"		"pistol"
	"bucket_position"	"4"

	"clip_size"		"8"
	"primary_ammo"		"357"
	"secondary_ammo"	"None"

	"item_flags"		"0"
	
	"CanIronsight"	"1"
	"IronSight"
	{
		"forward"	"-2"		//x
		"right"		"-5.45"		//y
		"up"		"2.3"		//z
		"pitch"		"0"
		"yaw"		"0"
		"roll"		"1.6"
		"fov"		"-20"
	}

	SoundData // TODO sounds
	{
		"reload"		"Weapon_Deagle.Reload"
		"reload_npc"		"Weapon_Pistol.Reload"

		"empty"			"Weapon_Pistol.Empty"

		"single_shot"		"Weapon_Deagle.Single"
		"single_shot_npc"	"Weapon_Deagle.NPC_Single"//Weapon_357.Single
	}

	TextureData
	{
		"weapon"
		{
				"font"		"SMODWeaponIcons"
				"character"	"q"
		}
		"weapon_s"
		{	
				"font"		"SMODWeaponIconsSelected"
				"character"	"q"
		}
		"weapon_small"
		{
				"font"		"SMODWeaponIconsSmall"
				"character"	"q"
		}
		"ammo"
		{
			"font"		"WeaponIcons"
			"character"	"q"
		}
		"crosshair"
		{
			"font"		"Crosshairs"
			"character"	"Q"
		}
		"autoaim"
		{
			"file"		"sprites/crosshairs"
			"x"		"0"
			"y"		"48"
			"width"		"24"
			"height"	"24"
		}
	}
}