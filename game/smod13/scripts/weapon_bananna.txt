// Bananna!

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"#SMOD_Bananna"
	"viewmodel"			"models/weapons/v_banana.mdl"
	"playermodel"		"models/weapons/w_banana.mdl"
	"anim_prefix"		"Grenade"
	"bucket"			"grenades"
	"bucket_position"	"4"

	"clip_size"			"-1"
	"clip2_size"		"-1"
	"default_clip"		"1"
	"default_clip2"		"-1"

	"primary_ammo"		"grenade"
	"secondary_ammo"	"None"

	"item_flags"		"18"

	"DisableROAimmode"	"1"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"special1"		"Weapon_Pistol.Special1"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
			"file"		"sprites/hud_weapons"
			"x"		"0"
			"y"		"64"
			"width"		"64"
			"height"	"32"
		}
		"weapon_s"
		{
			"file"		"sprites/hud_weapons"
			"x"		"0"
			"y"		"64"
			"width"		"64"
			"height"	"32"
		}
		"ammo"
		{
			"font"		"WeaponIcons"
			"character"	"Z"
		}
		"crosshair"
		{
			"file"		"sprites/hud_weapons"
			"x"		"0"
			"y"		"0"
			"width"		"64"
			"height"	"32"
		}
		"autoaim"
		{
			"file"		"sprites/crosshairs"
			"x"		"0"
			"y"		"48"
			"width"		"24"
			"height"	"24"
		}
	}
}
