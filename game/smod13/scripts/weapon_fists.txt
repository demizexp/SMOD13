// Crowbar

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"				"#SMOD_Fists"
	"viewmodel"				"models/weapons/v_fists.mdl"
	"playermodel"			"models/null.mdl"
	"anim_prefix"			"crowbar"
	"bucket"				"melee"
	"bucket_position"		"0"

	"clip_size"				"-1"
	"primary_ammo"			"None"
	"secondary_ammo"		"None"

	"weight"				"0"
	"item_flags"			"0"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"single_shot"		"Weapon_Crowbar.Single"
		"melee_hit"			"Weapon_Crowbar.Melee_Hit"
		"melee_hit_world"	"Weapon_Crowbar.Melee_HitWorld"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
			"file"		"sprites/crosshairs"
			"x"			"0"
			"y"			"48"
			"width"		"24"
			"height"	"24"
		}
	}
}