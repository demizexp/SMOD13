// Pistol

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"	"#HL2_Pistol"
	"viewmodel"			"models/weapons/v_pistol.mdl"
	"playermodel"		"models/weapons/w_pistol.mdl"
	"anim_prefix"		"pistol"
	"bucket"			"pistol"
	"bucket_position"	"0"
	"bucket_360"			"0"
	"bucket_position_360"	"0"
	
	"clip_size"			"18"
	"primary_ammo"		"Pistol"
	"secondary_ammo"	"None"

	"item_flags"		"0"
	
	"CanIronsight"	"1"
	"IronSight"
	{
		"forward"	"-4"		//x
		"right"		"-5.92"		//y
		"up"		"4.15"		//z
		"pitch"		"0"
		"yaw"		"-1.4"
		"roll"		"2"
		"fov"		"-20"
	}

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{

		"reload"		"Weapon_Pistol.Reload"
		"reload_npc"	"Weapon_Pistol.Reload"
		"empty"			"Weapon_Pistol.Empty"
		"single_shot"	"Weapon_Pistol.Single"
		"single_shot_npc"	"Weapon_Pistol.Single"
		"special1"		"Weapon_Pistol.Special1"
		"special2"		"Weapon_Pistol.Special2"
		"burst"			"Weapon_Pistol.Burst"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"SMODWeaponIcons"
				"character"	"a"
		}
		"weapon_s"
		{	
				"font"		"SMODWeaponIconsSelected"
				"character"	"a"
		}
		"weapon_small"
		{
				"font"		"SMODWeaponIconsSmall"
				"character"	"a"
		}
		"ammo"
		{
				"font"		"WeaponIconsSmall"
				"character"	"p"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}